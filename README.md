https://softchris.github.io/pages/dotnet-dockerize.html#build-our-image-start-container
https://medium.com/@sanketmeghani/docker-transferring-docker-images-without-registry-2ed50726495f

create DockerFile
create .dockerignore


to build images
docker build -t <imageName> .


docker images
docker run -d -p <hostport>:<dockerport>--name <appalias> <imagename>

to save to .tar
docker save --output <saved-image-name>.tar netcoremvc:latest

to load docker
docker load --input saved-image.tar